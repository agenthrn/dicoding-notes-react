import React from "react";
import NotesInput from "./NotesInput";
import { getInitialData } from "../utils/index";
import NotesList from "./NotesList";
import NotesHeader from "./NotesHeader";

class NotesApp extends React.Component {
  constructor(props) {
    super(props);
    const notes = getInitialData();
    this.state = {
      searchKeyword: "",
      notes: notes,
      filteredNotes: notes,
    };

    // binding event handler
    this.onSearchEventHandler = this.onSearchEventHandler.bind(this);
    this.onDeleteHandler = this.onDeleteHandler.bind(this);
    this.onArchiveHandler = this.onArchiveHandler.bind(this);
    this.onAddNoteHandler = this.onAddNoteHandler.bind(this);
  }

  onSearchEventHandler({ keyword }) {
    let filteredNotes = this.state.notes.filter(
      (e) => e.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1
    );
    this.setState(() => {
      return {
        filteredNotes: filteredNotes,
        searchKeyword: keyword,
      };
    });
  }

  onDeleteHandler(id) {
    const filteredNotes = this.state.filteredNotes.filter((e) => e.id !== id);
    const notes = this.state.notes.filter((e) => e.id !== id);
    this.setState({ filteredNotes, notes });
  }

  onArchiveHandler(id) {
    const index = this.state.notes.findIndex((e) => e.id === id);
    const notes = this.state.notes;
    notes[index]["archived"] = notes[index]["archived"] ? false : true;
    this.setState({ notes });
  }

  onAddNoteHandler({ title, body }) {
    const time = new Date();
    this.setState((prevState) => {
      const filteredNotes = [
        ...prevState.filteredNotes,
        {
          id: time,
          title,
          body,
          archived: false,
          createdAt: time.toISOString(),
        },
      ];
      return {
        filteredNotes: filteredNotes,
        notes: filteredNotes,
      };
    });
  }

  render() {
    return (
      <>
        <NotesHeader searchNote={this.onSearchEventHandler} />
        <div className="note-app__body">
          <NotesInput addNote={this.onAddNoteHandler} />
          <h2>Catatan Aktif</h2>
          <NotesList
            onDelete={this.onDeleteHandler}
            onArchive={this.onArchiveHandler}
            notes={this.state.filteredNotes.filter(
              (note) => note.archived === false
            )}
          />
          <h2>Arsip</h2>
          <NotesList
            onDelete={this.onDeleteHandler}
            onArchive={this.onArchiveHandler}
            notes={this.state.filteredNotes.filter(
              (note) => note.archived === true
            )}
          />
        </div>
      </>
    );
  }
}
export default NotesApp;
