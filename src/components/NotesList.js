import React from "react";
import NotesItem from "./NotesItem";

function NotesList({ notes, onDelete, onArchive }) {
  return notes.length > 0 ? (
    <div className="notes-list">
      {notes.map((data) => (
        <NotesItem
          onDelete={onDelete}
          onArchive={onArchive}
          key={data.id}
          {...data}
        />
      ))}
    </div>
  ) : (
    <p className="notes-list__empty-message">Tidak ada catatan</p>
  );
}
export default NotesList;
