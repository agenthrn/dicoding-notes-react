import React from "react";

class NotesHeader extends React.Component {
  constructor(props) {
    super(props);

    // inisialisasi state
    this.state = {
      keyword: "",
    };

    this.onKeywordChangeEventHandler =
      this.onKeywordChangeEventHandler.bind(this);
  }
  onKeywordChangeEventHandler(event) {
    let keyword = event.target.value;
    this.setState({ keyword: keyword });
    this.props.searchNote({ keyword: keyword });
  }
  render() {
    return (
      <div className="note-app__header">
        <h1>Catatanku</h1>
        <div className="note-search">
          <input
            type="text"
            placeholder="Cari catatan"
            onChange={this.onKeywordChangeEventHandler}
            value={this.state.keyword}
          />
        </div>
      </div>
    );
  }
}
export default NotesHeader;
