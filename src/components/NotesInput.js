import React from "react";

class NotesInput extends React.Component {
  constructor(props) {
    super(props);

    // inisialisasi state
    this.state = {
      title: "",
      body: "",
      bodyCounter: 0,
    };

    this.onTitleChangeEventHandler = this.onTitleChangeEventHandler.bind(this);
    this.onBodyChangeEventHandler = this.onBodyChangeEventHandler.bind(this);
    this.onSubmitEventHandler = this.onSubmitEventHandler.bind(this);
  }

  onTitleChangeEventHandler(event) {
    this.setState((prevState) => {
      return {
        ...prevState,
        title: event.target.value,
      };
    });
  }

  onBodyChangeEventHandler(event) {
    let bodyText = event.target.value;
    if (bodyText.length <= 50) {
      this.setState((prevState) => {
        return {
          ...prevState,
          body: bodyText,
          bodyCounter: bodyText.length,
        };
      });
    }
  }

  onSubmitEventHandler(event) {
    event.preventDefault();
    this.props.addNote(this.state);
  }

  render() {
    return (
      <div className="note-app__body">
        <div className="note-input">
          <h2>Buat Catatan</h2>
          <form className="contact-input" onSubmit={this.onSubmitEventHandler}>
            <p className="note-input__title__char-limit">
              Sisa karakter:{50 - this.state.bodyCounter}
            </p>
            <input
              type="text"
              className="note-input__title"
              placeholder="Masukan judul disini"
              onChange={this.onTitleChangeEventHandler}
              value={this.state.title}
              required
            />
            <textarea
              className="note-input__body"
              placeholder="Masukan catatan disini"
              onChange={this.onBodyChangeEventHandler}
              value={this.state.body}
              required
            ></textarea>
            <button type="submit">Buat</button>
          </form>
        </div>
      </div>
    );
  }
}

export default NotesInput;
